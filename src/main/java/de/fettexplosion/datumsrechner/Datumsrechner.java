package de.fettexplosion.datumsrechner;

import java.util.Locale;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public class Datumsrechner {
	
	private static final int DEFAULT_SCOPE = 40000;
	private static final int DEFAULT_INTERVAL = 1000;
	private static final int DEFAULT_ITERATIONS = DEFAULT_SCOPE / DEFAULT_INTERVAL;
	private static final DateTime DEFAULT_START_DATE = new DateTime(1973, 8, 23, 0, 0);
	private static final DateTimeFormatter FULL_DATE_FORMATTER = DateTimeFormat.fullDate().withLocale(Locale.GERMAN);

	public void calculateInterval(final DateTime start,final  int intervalSize, final int iterations) {
		DateTime iterDate = new DateTime(start);
		for (int i = 1; i < iterations; i++) {
			iterDate = iterDate.plusDays(intervalSize);
			System.out.println(i * intervalSize + " Tage: " + FULL_DATE_FORMATTER.print(iterDate));
		}
	}
	
	public static void main(String[] args) {
		Datumsrechner dr = new Datumsrechner();
		DateTime cmdLineDate = getCmdLineDate();
		DateTime startDate = cmdLineDate != null ? cmdLineDate : DEFAULT_START_DATE;
		dr.calculateInterval(startDate, DEFAULT_INTERVAL, DEFAULT_ITERATIONS);
	}

}
